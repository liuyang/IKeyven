package _Keyven;

import java.io.IOException;
import java.util.ArrayList;

import org.ansj.splitWord.analysis.ToAnalysis;

import stsg.code.ISTSG;
import stsg.code.STSGFactory;
import stsg.code.STSGType;
import util.FileUtil;
import dependency.code.DepObject;
import dependency.code.Dependency;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;

public class MainTest1 {
	public static void main(String[] args) {
		StanfordParser.lp = LexicalizedParser
				.loadModel("lib/chinesePCFG.ser.gz");
		UserDefine.addwords();
		ToAnalysis.parse("");
		ArrayList<String> list = new ArrayList<>();
		try {
			list = FileUtil.readFile("src/_Keyven/hankcs.txt", "gbk");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		boolean choice = false;
		for (String str : list) {
			String oris = str;
			System.out.print(str + "\t");
			ISTSG user = STSGFactory.stsgproducer(STSGType.COMPRESSION);
			str = user.synchronous_tree_substitution_grammar(str);
			UserDefine.addtemps();
			Dependency temp = new Dependency();
			DepObject obj = temp.process(str, choice);
			if (obj.isUseless()) {
				String ans = Dependency.repair(oris, str);
				System.out.println(ans);
			} else {
				String ans = Dependency.repair(oris, obj.toString());
				if (Dependency.isCC(obj, str)) {
					System.out.println(str);
				} else {
					System.out.println(ans);
				}
			}
			UserDefine.removetemps();
		}
	}
}
